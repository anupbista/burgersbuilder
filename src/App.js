import React, { Component } from 'react';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import Layout from './components/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Logout from './containers/Auth/Logout/Logout';
import Auth from './containers/Auth/Auth';
import * as actions from './store/actions/index';
import asyncComponent from './hoc/asyncComponent/asyncComponent';

const asyncCheckout = asyncComponent(()=> {
  return import ('./containers/Checkout/Checkout')
});

const asyncOrders = asyncComponent(()=> {
  return import ('./containers/Orders/Orders')
});

class App extends Component {
  componentDidMount(){
    this.props.onAutoSignUp();
  }

  render() {
    
    let routes = (
      <Switch>
          <Route path="/auth" component={Auth} />
          <Route path="/" exact component={BurgerBuilder} />
          <Redirect to="/"/>
        </Switch>
    );

    if(this.props.isAuth){
      routes=(
        <Switch>
            <Route path="/checkout" component={asyncCheckout} />
            <Route path="/orders" component={asyncOrders} />
            <Route path="/logout" component={Logout} />
            <Route path="/auth" component={Auth} />            
            <Route path="/" exact component={BurgerBuilder} />
            <Redirect to="/"/>            
        </Switch>
      );
    }

    return (
      <div>
          <Layout>
            {routes}
          </Layout>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isAuth: state.auth.token !== null
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    onAutoSignUp: () => {
      dispatch(actions.authCheckState())
    }
  }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps )(App));