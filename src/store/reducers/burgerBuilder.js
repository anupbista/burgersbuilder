import * as actionTypes from './../actions/actionTypes';
import { updateObject } from '../utiliity';

const initialState= {
    ingredients: null,
    totalPrice: 4,
    error: false,
    building: false
};

const INGREDIENTS_PRICES = {
    cheese: 0.4,
    bacon: 0.3,
    meat: 1.2,
    salad: 0.9
}

const addIngredient = (state, action) => {
    const updatedIngredinet = {
        [action.ingredientName]: state.ingredients[action.ingredientName] + 1
    }
    const updatedIngredinets = updateObject(state.ingredients, updatedIngredinet);
    const updatedState = {
        ingredients: updatedIngredinets,
        totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingredientName],
        building: true
    }
    return updateObject(state, updatedState);
}

const removeIngredient = (state, action) => {
    const updatedIng = {
        [action.ingredientName]: state.ingredients[action.ingredientName] - 1
    }
    const updatedIngs = updateObject(state.ingredients, updatedIng);
    const updatedSt = {
        ingredients: updatedIngs,
        totalPrice: state.totalPrice - INGREDIENTS_PRICES[action.ingredientName],
        building: true
    }
    return updateObject(state, updatedSt);
}

const setIngredients = (state, action) => {
    return updateObject(state,{
        ingredients: {
            salad: action.ingredients.salad,
            bacon: action.ingredients.bacon,
            cheese: action.ingredients.cheese,
            meat: action.ingredients.meat
        },
        totalPrice: 4,
        error: false,
        building: false
    });
}

const fetchIngredientFailed = (state, action) => {
    return updateObject(state,{
        error: true            
    } );
}

const reducer = ( state = initialState, action ) => {
    switch(action.type){
        case actionTypes.ADD_INGREDIENT: return addIngredient(state, action);
        case actionTypes.REMOVE_INGREDIENT: return removeIngredient(state,action);
        case actionTypes.SET_INGREDIENTS: return setIngredients(state, action);
        case actionTypes.FETCH_INGREDIENT_FAILED: return fetchIngredientFailed(state, action);
        default: return state;
    }
};

export default reducer;