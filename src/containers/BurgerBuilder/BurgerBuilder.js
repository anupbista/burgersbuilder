import React, {Component} from 'react';
import { connect } from 'react-redux';

import Aux from '../../hoc/Aux';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummnary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import * as burderBuilderActions from '../../store/actions/index';
import axios from '../../axios-order';

class BurgerBuilder extends Component {

    state = {
        purchasing: false,
    }

    componentDidMount(){
        this.props.onInitIngredients() ;
    }

    updatePurchaseState = (ingredients) => {
        const sum = Object.keys(ingredients).map( igkey=>{
            return ingredients[igkey];
        } ).reduce((sum, el)=>{
            return sum + el;
        }, 0);
        
        return sum > 0
       
    }

    puchaseHandler = () => {
        if(this.props.isAuth){
            this.setState({purchasing:true});
        }
        else{
            this.props.onSetAuthRedirectPath('/checkout');
            this.props.history.push('/auth');
        }
    }

    purchaseCancelHandler = () => {
        this.setState({
             purchasing: false
         })
    }

    purchaseContinueHandler = () =>{
        this.props.onInitPurchase();
        this.props.history.push('/checkout');
    }

    render() {
        const disabledInfo={
            ...this.props.ings
        };
        for(let key in disabledInfo){
            disabledInfo[key] = disabledInfo[key] <= 0;
        }
        let OrderSummary = null;

         let burger = this.props.error ? <p>Ingredients can't be loaded</p> : <Spinner />;

         if(this.props.ings){
            burger = (
                <Aux>
                   <Burger ingredients={this.props.ings}/>
                   <BuildControls ingredientAdded={this.props.onIngredientAdded} 
                   ingredientRemoved={this.props.onIngredientRemoved}
                   disabled={disabledInfo}
                   purchaseable={this.updatePurchaseState(this.props.ings)}
                   isAuth={this.props.isAuth}
                   order={this.puchaseHandler}
                   price={this.props.price}/>
               </Aux> 
            );
            OrderSummary = <OrderSummnary 
            totalPrice={this.props.price}
            ingredients={this.props.ings}
            purchaseCancel={this.purchaseCancelHandler}
            purchaseContinue={this.purchaseContinueHandler}
             />;
        }

      return (
          <Aux>
              <Modal show={this.state.purchasing} modalClosed={this.purchaseCancelHandler}>
                  {OrderSummary}
              </Modal>
              {burger}
          </Aux>
      );
    }
}

const mapStateToProps = (state) => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuth: state.auth.token !== null
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
       onIngredientAdded: (ingName) => dispatch(burderBuilderActions.addIngredient(ingName)),
       onIngredientRemoved: (ingName) => dispatch(burderBuilderActions.removeIngredient(ingName)),
       onInitIngredients: () => dispatch(burderBuilderActions.initIngredients()),
       onInitPurchase: () => dispatch(burderBuilderActions.purchaseInit()),
       onSetAuthRedirectPath: (path) => dispatch(burderBuilderActions.setAuthRedirectPath(path)) 
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));